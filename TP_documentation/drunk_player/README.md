# Drunk Player

## Description

- opencv
- boost

## Utilisation

```bash
./drunk_player_gui.out ../data
```

![](drunk_player_gui.png)

[dépôt](https://gitlab.com/Ocean62/cicd)

lien explicite: <https://gitlab.com/Ocean62/cicd>

code inline `int main() {return 0;}`

*italique*

**gras**

> bloc
> de code

- item
- item:
	- sous-item
	- sous-item