#!/bin/sh
g++ -std=c++11 -Wall -Wextra -O2 -c module1.cpp
g++ -std=c++11 -Wall -Wextra -O2 -c sayHello.cpp
g++ -std=c++11 -Wall -Wextra -O2 -o sayHello module1.o sayHello.o
